18.0.0 Update to Angular 18.
16.0.2 Update visualization versions.
16.0.1 Update sentinel versions.
16.0.0 Update to Angular 16 and update local issuer to keycloak.
15.0.0 Update to Angular 15.
14.3.0 Bump clustering visualization to address changes in max achievable score.
14.2.0 Bump hurdling visualization and address changes in inputs.
14.1.0 Remove elastic search service url.
14.0.1 Rename kypo2 to kypo.
14.0.0 Update to Angular 14.
13.0.1 Fix problems with timeline in clustering and line visualizations.
13.0.0 Update to Angular 13, CI/CD update.
12.1.0 Fix problems with trainee selection, zoom reset and Walkthrough title.
12.0.3 Responsivity problems, interactions between visualizations adjusted.
12.0.2 Fix style issues.
12.0.1 Refactored visualization dashboard. Added Summary and Reference graph component for definitions with reference solutions.
